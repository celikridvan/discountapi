Retail Store Discounts Api
=================================================
This api provides net payable amount of requested bill.

Table of contents
-----------------

* [Prequisites](#prequisites)
* [Installation](#installation)
* [Usage](#usage)
* [Acknowledgments](#acknowledgments)

Prequisites
------------

- [Java 17](https://www.oracle.com/java/technologies/javase/jdk17-readme-downloads.html)
- [Docker](https://www.docker.com/products/docker-desktop)
- [Apache Maven](https://github.com/apache/maven/blob/master/README.md)
- [Sonar Qube](https://github.com/SonarSource/sonarqube/blob/master/README.md)
  
Installation
------------
To build application, open command prompt and enter:

```
mvn clean install
```

Then, we need to create docker image by typing:

```
docker build -t discount-api .
```

To start application:

```
docker-compose up
```

To check application started successfully ``docker ps`` command should result:
```
CONTAINER ID   IMAGE              COMMAND                  CREATED          STATUS          PORTS                      NAMES
aed2b0b4f112   discount-api       "java -jar /app.jar"     41 seconds ago   Up 40 seconds   0.0.0.0:8080->8080/tcp     discount-api
c88d5b12c785   mongo:latest       "docker-entrypoint.s…"   43 seconds ago   Up 42 seconds   0.0.0.0:27017->27017/tcp   mongodb
c610da1bdfdb   sonarqube:latest   "/opt/sonarqube/bin/…"   43 seconds ago   Up 42 seconds   0.0.0.0:9000->9000/tcp     sonarqube
```

Run analysis on project:
```
mvn sonar:sonar
```

Usage
------------

Curl Request Example:
```
curl --location --request POST 'http://localhost:8080/api/getPayableAmount' \
--header 'Authorization: Basic dXNlckVtcGxveWVlOnBhc3N3b3Jk' \
--header 'Content-Type: application/json' \
--header 'Cookie: JSESSIONID=37CC7D092E2E9EED0E5992C9083D0773' \
--data-raw '{
    "billItems":[
        {
            "itemType":"GROCERIES",
            "amount":500
        },
        {
            "itemType":"ELECTRONIC",
            "amount":500
        }
    ]
}'
```

Postman Collection:
[Local.postman_collection.json](doc/Local.postman_collection.json)

Swagger UI Url:
```
http://localhost:8080/swagger-ui/index.html
```

SonarQube
```
http://localhost:9000/projects
```

Acknowledgments
------------
UML Diagram:
![Alt text](doc/discountApi.umlcd.png?raw=true "UML Diagram")
