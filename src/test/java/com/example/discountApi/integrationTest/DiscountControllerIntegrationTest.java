package com.example.discountApi.integrationTest;

import com.example.discountApi.DiscountApiApplication;
import com.example.discountApi.controller.DiscountController;
import com.example.discountApi.controller.request.BillItem;
import com.example.discountApi.controller.request.DiscountRequest;
import com.example.discountApi.controller.request.ItemType;
import com.example.discountApi.controller.response.DiscountResponse;
import com.example.discountApi.entity.User;
import com.example.discountApi.entity.UserType;
import com.example.discountApi.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@SpringBootTest(classes = {DiscountApiApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureDataMongo
@AutoConfigureMockMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class DiscountControllerIntegrationTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private DiscountController discountController;
    private Faker faker;
    private ObjectMapper objectMapper;
    @Autowired
    private UserRepository userRepository;

    @BeforeEach
    void setUp() {
        this.faker = new Faker();
        this.objectMapper = new ObjectMapper();
    }

    @Test
    public void contextLoads() throws Exception {
        assertThat(discountController).isNotNull();
    }

    @Test
    public void getPayableAmount_should_throw_exception_user_not_found() throws Exception {
        createUsers();

        String userName = faker.name().username();
        DiscountRequest discountRequest = DiscountRequest.builder()
                .billItems(Arrays.asList(BillItem.builder().amount(BigDecimal.valueOf(1000)).itemType(ItemType.GROCERIES).build(),
                        BillItem.builder().amount(BigDecimal.valueOf(1000)).itemType(ItemType.ELECTRONIC).build()))
                .build();
        MvcResult mvcResult = this.mockMvc.perform(post("/api/getPayableAmount")
                        .content(objectMapper.writeValueAsString(discountRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .with(httpBasic(userName, "password")))
                .andReturn();

        assertThat(mvcResult).isNotNull();
        assertThat(mvcResult.getResponse().getErrorMessage()).isNotNull();
        assertThat(mvcResult.getResponse().getErrorMessage()).contains("Unauthorized");
    }

    @Test
    public void getPayableAmount_should_apply_employee() throws Exception {
        createUsers();

        DiscountRequest discountRequest = DiscountRequest.builder()
                .billItems(Arrays.asList(BillItem.builder().amount(BigDecimal.valueOf(1000)).itemType(ItemType.GROCERIES).build(),
                        BillItem.builder().amount(BigDecimal.valueOf(1000)).itemType(ItemType.ELECTRONIC).build()))
                .build();
        MvcResult mvcResult = this.mockMvc.perform(post("/api/getPayableAmount")
                        .content(objectMapper.writeValueAsString(discountRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .with(httpBasic("userEmployee", "password")))
                .andReturn();

        assertThat(mvcResult).isNotNull();
        assertThat(mvcResult.getResponse().getStatus()).isEqualTo(200);
        DiscountResponse discountResponse = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), DiscountResponse.class);
        assertThat(discountResponse).isNotNull();
        assertThat(discountResponse.getNetPayable()).isEqualByComparingTo(BigDecimal.valueOf(1615));
    }

    @Test
    public void getPayableAmount_should_apply_affiliate() throws Exception {
        createUsers();

        DiscountRequest discountRequest = DiscountRequest.builder()
                .billItems(Arrays.asList(BillItem.builder().amount(BigDecimal.valueOf(1000)).itemType(ItemType.GROCERIES).build(),
                        BillItem.builder().amount(BigDecimal.valueOf(1000)).itemType(ItemType.ELECTRONIC).build()))
                .build();
        MvcResult mvcResult = this.mockMvc.perform(post("/api/getPayableAmount")
                        .content(objectMapper.writeValueAsString(discountRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .with(httpBasic("userAffiliate", "password")))
                .andReturn();

        assertThat(mvcResult).isNotNull();
        assertThat(mvcResult.getResponse().getStatus()).isEqualTo(200);
        DiscountResponse discountResponse = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), DiscountResponse.class);
        assertThat(discountResponse).isNotNull();
        assertThat(discountResponse.getNetPayable()).isEqualByComparingTo(BigDecimal.valueOf(1805));
    }

    @Test
    public void getPayableAmount_should_apply_standard_user() throws Exception {
        createUsers();

        DiscountRequest discountRequest = DiscountRequest.builder()
                .billItems(Arrays.asList(BillItem.builder().amount(BigDecimal.valueOf(1000)).itemType(ItemType.GROCERIES).build(),
                        BillItem.builder().amount(BigDecimal.valueOf(1000)).itemType(ItemType.ELECTRONIC).build()))
                .build();
        MvcResult mvcResult = this.mockMvc.perform(post("/api/getPayableAmount")
                        .content(objectMapper.writeValueAsString(discountRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .with(httpBasic("userStandard", "password")))
                .andReturn();

        assertThat(mvcResult).isNotNull();
        assertThat(mvcResult.getResponse().getStatus()).isEqualTo(200);
        DiscountResponse discountResponse = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), DiscountResponse.class);
        assertThat(discountResponse).isNotNull();
        assertThat(discountResponse.getNetPayable()).isEqualByComparingTo(BigDecimal.valueOf(1900));
    }

    @Test
    public void getPayableAmount_should_apply_old_user() throws Exception {
        createUsers();

        DiscountRequest discountRequest = DiscountRequest.builder()
                .billItems(Arrays.asList(BillItem.builder().amount(BigDecimal.valueOf(1000)).itemType(ItemType.GROCERIES).build(),
                        BillItem.builder().amount(BigDecimal.valueOf(1000)).itemType(ItemType.ELECTRONIC).build()))
                .build();
        MvcResult mvcResult = this.mockMvc.perform(post("/api/getPayableAmount")
                        .content(objectMapper.writeValueAsString(discountRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .with(httpBasic("userOld", "password")))
                .andReturn();

        assertThat(mvcResult).isNotNull();
        assertThat(mvcResult.getResponse().getStatus()).isEqualTo(200);
        DiscountResponse discountResponse = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), DiscountResponse.class);
        assertThat(discountResponse).isNotNull();
        assertThat(discountResponse.getNetPayable()).isEqualByComparingTo(BigDecimal.valueOf(1805));
    }

    @AfterEach
    public void afterEach() {
        userRepository.deleteAll();
    }

    private void createUsers() {
        userRepository.deleteAll();
        userRepository.save(User.builder()
                .userName("userEmployee")
                .password(passwordEncoder.encode("password"))
                .userType(UserType.EMPLOYEE)
                .build());
        userRepository.save(User.builder()
                .userName("userAffiliate")
                .password(passwordEncoder.encode("password"))
                .userType(UserType.AFFLILIATE)
                .build());
        userRepository.save(User.builder()
                .userName("userStandard")
                .password(passwordEncoder.encode("password"))
                .userType(UserType.USER)
                .build());
        User user = userRepository.save(User.builder()
                .userName("userOld")
                .password(passwordEncoder.encode("password"))
                .userType(UserType.USER)
                .build());
        user.setCreationDate(LocalDate.now().minusYears(3));
        userRepository.save(user);
    }
}
