package com.example.discountApi.serviceTest;

import com.example.discountApi.controller.request.BillItem;
import com.example.discountApi.controller.request.DiscountRequest;
import com.example.discountApi.controller.request.ItemType;
import com.example.discountApi.discountengine.*;
import com.example.discountApi.entity.User;
import com.example.discountApi.entity.UserType;
import com.example.discountApi.exception.UserNotFoundException;
import com.example.discountApi.repository.UserRepository;
import com.example.discountApi.service.DiscountService;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DiscountServiceTest {

    private Faker faker;
    @Mock
    private UserRepository userRepository;
    @Mock
    private DiscountFactory discountFactory;
    @Mock
    private AffiliateDiscount affiliateDiscount;
    @Mock
    private CustomerOverYearDiscount customerOverYearDiscount;
    @Mock
    private DefaultDiscount defaultDiscount;
    @Mock
    private EmployeeDiscount employeeDiscount;
    @InjectMocks
    private DiscountService discountService;


    @BeforeEach
    void setUp() {
        this.faker = new Faker();
    }

    @Test
    void applyDiscount_should_throw_user_not_found_exception() {
        DiscountRequest discountRequest = DiscountRequest.builder()
                .billItems(Arrays.asList(
                        BillItem.builder().itemType(ItemType.GROCERIES).amount(BigDecimal.valueOf(500)).build(),
                        BillItem.builder().itemType(ItemType.ELECTRONIC).amount(BigDecimal.valueOf(500)).build()
                ))
                .build();
        String randomUserName = "Test";
        when(userRepository.findByUserName(anyString())).thenReturn(Optional.empty());
        Exception exception = assertThrows(Exception.class, () -> discountService.applyDiscount(discountRequest, randomUserName));
        assertThat(exception).isNotNull();
        assertThat(exception).isInstanceOf(UserNotFoundException.class);
        UserNotFoundException userNotFoundException = (UserNotFoundException) exception;
        assertThat(userNotFoundException.getMessage()).isEqualTo(String.format("userName: %s not found!", randomUserName));
    }

    @Test
    void applyDiscount_customer_over_year_discount_should_apply() {
        DiscountRequest discountRequest = DiscountRequest.builder()
                .billItems(Arrays.asList(
                        BillItem.builder().itemType(ItemType.GROCERIES).amount(BigDecimal.valueOf(500)).build(),
                        BillItem.builder().itemType(ItemType.ELECTRONIC).amount(BigDecimal.valueOf(500)).build()
                ))
                .build();
        User user = User.builder()
                .userName(faker.name().name())
                .userType(UserType.USER)
                .creationDate(LocalDate.now().minusYears(4))
                .build();
        BigDecimal amount = BigDecimal.valueOf(faker.number().randomDigitNotZero());

        when(userRepository.findByUserName(any())).thenReturn(Optional.of(user));
        when(discountFactory.get(any(User.class))).thenReturn(customerOverYearDiscount);
        when(customerOverYearDiscount.applyDiscount(any())).thenReturn(amount);
        BigDecimal netPayable = discountService.applyDiscount(discountRequest, user.getUserName());
        assertThat(netPayable).isNotNull();
        assertThat(netPayable).isPositive();
        assertThat(netPayable).isEqualByComparingTo(amount);
    }

    @Test
    void applyDiscount_default_discount_should_apply() {
        DiscountRequest discountRequest = DiscountRequest.builder()
                .billItems(Arrays.asList(
                        BillItem.builder().itemType(ItemType.GROCERIES).amount(BigDecimal.valueOf(500)).build(),
                        BillItem.builder().itemType(ItemType.ELECTRONIC).amount(BigDecimal.valueOf(500)).build()
                ))
                .build();
        User user = User.builder()
                .userName(faker.name().name())
                .userType(UserType.USER)
                .creationDate(LocalDate.now())
                .build();
        BigDecimal amount = BigDecimal.valueOf(faker.number().randomDigitNotZero());

        when(userRepository.findByUserName(any())).thenReturn(Optional.of(user));
        when(discountFactory.get(any(User.class))).thenReturn(defaultDiscount);
        when(defaultDiscount.applyDiscount(any())).thenReturn(amount);
        BigDecimal netPayable = discountService.applyDiscount(discountRequest, user.getUserName());
        assertThat(netPayable).isNotNull();
        assertThat(netPayable).isPositive();
        assertThat(netPayable).isEqualByComparingTo(amount);
    }

    @Test
    void applyDiscount_employee_discount_should_apply() {
        DiscountRequest discountRequest = DiscountRequest.builder()
                .billItems(Arrays.asList(
                        BillItem.builder().itemType(ItemType.GROCERIES).amount(BigDecimal.valueOf(500)).build(),
                        BillItem.builder().itemType(ItemType.ELECTRONIC).amount(BigDecimal.valueOf(500)).build()
                ))
                .build();
        User user = User.builder()
                .userName(faker.name().name())
                .userType(UserType.EMPLOYEE)
                .creationDate(LocalDate.now())
                .build();
        BigDecimal amount = BigDecimal.valueOf(faker.number().randomDigitNotZero());

        when(userRepository.findByUserName(any())).thenReturn(Optional.of(user));
        when(discountFactory.get(any(User.class))).thenReturn(employeeDiscount);
        when(employeeDiscount.applyDiscount(any())).thenReturn(amount);
        BigDecimal netPayable = discountService.applyDiscount(discountRequest, user.getUserName());
        assertThat(netPayable).isNotNull();
        assertThat(netPayable).isPositive();
        assertThat(netPayable).isEqualByComparingTo(amount);
    }

    @Test
    void applyDiscount_affiliate_discount_should_apply() {
        DiscountRequest discountRequest = DiscountRequest.builder()
                .billItems(Arrays.asList(
                        BillItem.builder().itemType(ItemType.GROCERIES).amount(BigDecimal.valueOf(500)).build(),
                        BillItem.builder().itemType(ItemType.ELECTRONIC).amount(BigDecimal.valueOf(500)).build()
                ))
                .build();
        User user = User.builder()
                .userName(faker.name().name())
                .userType(UserType.AFFLILIATE)
                .creationDate(LocalDate.now())
                .build();
        BigDecimal amount = BigDecimal.valueOf(faker.number().randomDigitNotZero());

        when(userRepository.findByUserName(any())).thenReturn(Optional.of(user));
        when(discountFactory.get(any(User.class))).thenReturn(affiliateDiscount);
        when(affiliateDiscount.applyDiscount(any())).thenReturn(amount);
        BigDecimal netPayable = discountService.applyDiscount(discountRequest, user.getUserName());
        assertThat(netPayable).isNotNull();
        assertThat(netPayable).isPositive();
        assertThat(netPayable).isEqualByComparingTo(amount);
    }

}
