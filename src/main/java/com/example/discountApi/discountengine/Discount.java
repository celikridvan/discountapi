package com.example.discountApi.discountengine;

import com.example.discountApi.controller.request.BillItem;

import java.math.BigDecimal;
import java.util.List;

public interface Discount {
    BigDecimal applyDiscount(List<BillItem> billItems);
}
