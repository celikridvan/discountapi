package com.example.discountApi.discountengine;

import com.example.discountApi.conf.DiscountConf;
import com.example.discountApi.controller.request.BillItem;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Component
@AllArgsConstructor
public class DefaultDiscount implements Discount {

    final DiscountConf discountConf;

    @Override
    public BigDecimal applyDiscount(List<BillItem> billItems) {
        BigDecimal amount = billItems.stream()
                .map(BillItem::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        BigDecimal billDiscountByAmount = amount.divide(BigDecimal.valueOf(discountConf.getBillDiscountByAmountThreshold()), RoundingMode.DOWN)
                .multiply(BigDecimal.valueOf(discountConf.getBillDiscountByAmountDiscountRate()));

        return amount.subtract(billDiscountByAmount);
    }
}
