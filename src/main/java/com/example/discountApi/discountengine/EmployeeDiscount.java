package com.example.discountApi.discountengine;

import com.example.discountApi.conf.DiscountConf;
import com.example.discountApi.controller.request.BillItem;
import com.example.discountApi.controller.request.ItemType;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Component
@AllArgsConstructor
public class EmployeeDiscount implements Discount {

    final DiscountConf discountConf;

    @Override
    public BigDecimal applyDiscount(List<BillItem> billItems) {
        BigDecimal amount = billItems.stream().map(item -> {
            if (item.getItemType().equals(ItemType.GROCERIES))
                return item.getAmount();
            else
                return item.getAmount().subtract(
                        item.getAmount()
                                .multiply(BigDecimal.valueOf(discountConf.getEmployeeDiscountRate()))
                                .divide(BigDecimal.valueOf(100), RoundingMode.DOWN)
                );
        }).reduce(BigDecimal.ZERO, BigDecimal::add);

        BigDecimal billDiscountByAmount = amount.divide(BigDecimal.valueOf(discountConf.getBillDiscountByAmountThreshold()), RoundingMode.DOWN)
                .multiply(BigDecimal.valueOf(discountConf.getBillDiscountByAmountDiscountRate()));

        return amount.subtract(billDiscountByAmount);
    }
}
