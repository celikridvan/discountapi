package com.example.discountApi.discountengine;

import com.example.discountApi.conf.DiscountConf;
import com.example.discountApi.entity.User;
import com.example.discountApi.entity.UserType;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class DiscountFactory {

    private final EmployeeDiscount employeeDiscount;
    private final AffiliateDiscount affiliateDiscount;
    private final CustomerOverYearDiscount customerOverYearDiscount;
    private final DefaultDiscount defaultDiscount;
    private final DiscountConf discountConf;

    public Discount get(User user) {
        if (user.getUserType().equals(UserType.EMPLOYEE)) {
            return employeeDiscount;
        } else if (user.getUserType().equals(UserType.AFFLILIATE)) {
            return affiliateDiscount;
        } else if (user.calculateTotalYear() >= discountConf.getCustomerDiscountByYearThreshold()) {
            return customerOverYearDiscount;
        }
        return defaultDiscount;
    }
}
