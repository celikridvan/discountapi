package com.example.discountApi.conf;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "discount")
@Data
public class DiscountConf {

    private int employeeDiscountRate;
    private int affiliateDiscountRate;
    private int customerDiscountByYearThreshold;
    private int customerDiscountByYearDiscountRate;
    private int billDiscountByAmountThreshold;
    private int billDiscountByAmountDiscountRate;
}
