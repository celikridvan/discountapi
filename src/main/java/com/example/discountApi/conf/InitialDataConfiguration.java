package com.example.discountApi.conf;

import com.example.discountApi.entity.User;
import com.example.discountApi.entity.UserType;
import com.example.discountApi.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDate;

@Configuration
@AllArgsConstructor
public class InitialDataConfiguration {

    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    @Bean
    CommandLineRunner runner() {
        return args -> {
            userRepository.deleteAll();
            userRepository.save(User.builder()
                    .userName("userEmployee")
                    .password(passwordEncoder.encode("password"))
                    .userType(UserType.EMPLOYEE)
                    .build());
            userRepository.save(User.builder()
                    .userName("userAffiliate")
                    .password(passwordEncoder.encode("password"))
                    .userType(UserType.AFFLILIATE)
                    .build());
            userRepository.save(User.builder()
                    .userName("userStandard")
                    .password(passwordEncoder.encode("password"))
                    .userType(UserType.USER)
                    .build());
            User user = userRepository.save(User.builder()
                    .userName("userOld")
                    .password(passwordEncoder.encode("password"))
                    .userType(UserType.USER)
                    .build());
            user.setCreationDate(LocalDate.now().minusYears(3));
            userRepository.save(user);
        };
    }
}
