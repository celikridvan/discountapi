package com.example.discountApi.service;

import com.example.discountApi.controller.request.DiscountRequest;
import com.example.discountApi.discountengine.Discount;
import com.example.discountApi.discountengine.DiscountFactory;
import com.example.discountApi.entity.User;
import com.example.discountApi.exception.UserNotFoundException;
import com.example.discountApi.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;

@Service
@AllArgsConstructor
@Slf4j
public class DiscountService {

    final DiscountFactory discountFactory;
    final UserRepository userRepository;

    public BigDecimal applyDiscount(DiscountRequest disCountRequest, String userName) {
        Optional<User> userOptional = userRepository.findByUserName(userName);
        if (!userOptional.isPresent()) {
            throw new UserNotFoundException(String.format("userName: %s not found!", userName));
        }
        User user = userOptional.get();
        Discount disCount = discountFactory.get(user);
        log.info(String.format("Applying Engine %s to request : %s", disCount.getClass().getSimpleName(), disCountRequest));
        return disCount.applyDiscount(disCountRequest.getBillItems());
    }
}
