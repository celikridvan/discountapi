package com.example.discountApi.entity;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.time.Period;

@Document
@Builder
@Data
public class User {

    @Id
    private String id;
    @Indexed(unique = true)
    private String userName;
    private String password;
    @CreatedDate
    private LocalDate creationDate;
    private UserType userType;

    public int calculateTotalYear() {
        return Period.between(this.getCreationDate(), LocalDate.now()).getYears();
    }

}
