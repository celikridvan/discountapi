package com.example.discountApi.controller.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BillItem {

    @DecimalMin(value = "1.00", message = "amount must be greater than or equal to 1.00")
    private BigDecimal amount;

    @NotNull(message = "itemType must not be null")
    private ItemType itemType;
}
