package com.example.discountApi.controller.request;

public enum ItemType {
    GROCERIES,
    ELECTRONIC,
    CLOTHES;

}
