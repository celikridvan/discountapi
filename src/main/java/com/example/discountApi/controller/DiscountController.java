package com.example.discountApi.controller;

import com.example.discountApi.controller.request.DiscountRequest;
import com.example.discountApi.controller.response.DiscountResponse;
import com.example.discountApi.service.DiscountService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.math.BigDecimal;

@RestController
@RequestMapping(value = "/api")
@AllArgsConstructor
@Slf4j
public class DiscountController {

    private final DiscountService discountService;

    @PostMapping(value = "/getPayableAmount", produces = "application/json")
    public DiscountResponse getPayableAmount(Authentication authentication, @RequestBody @Valid DiscountRequest disCountRequest) {
        log.info(String.format("getPayableAmount Received DiscountRequest: %s", disCountRequest));
        BigDecimal netPayable = discountService.applyDiscount(disCountRequest, authentication.getName());
        DiscountResponse discountResponse = DiscountResponse.builder()
                .netPayable(netPayable)
                .build();
        log.info(String.format("getPayableAmount DiscountResponse %s", discountResponse));
        return discountResponse;
    }
}
