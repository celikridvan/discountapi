package com.example.discountApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.config.EnableMongoAuditing;

@SpringBootApplication
@EnableMongoAuditing
public class DiscountApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscountApiApplication.class, args);
	}

}
